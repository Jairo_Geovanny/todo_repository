//
//  AddItemController.swift
//  ToDo
//
//  Created by JAIRO PROAÑO on 15/5/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import UIKit

class AddItemController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    var itemManager: ItemManager?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancel(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func save(_ sender: UIButton) {
        
        let itemTitle = titleTextField.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextField.text ?? ""
        
        let item = Item(
            title:  itemTitle,
            location: itemLocation,
            description: itemDescription
        )
        
        if item.title == "" {
            showAlert(title: "Error", message: "Title is required")
        }
        
        itemManager?.toDoItems += [item]
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}
