//
//  Item.swift
//  ToDo
//
//  Created by JAIRO PROAÑO on 15/5/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import Foundation

class Item {
    
    var title: String = ""
    var location: String = ""
    var description: String = ""
    
    init(title: String, location:String, description: String) {
        self.title = title
        self.location = location
        self.description = description
    }
    
}
