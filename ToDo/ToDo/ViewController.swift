//
//  ViewController.swift
//  ToDo
//
//  Created by JAIRO PROAÑO on 8/5/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    
    var itemManager = ItemManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination  = segue.destination as! AddItemController
        destination.itemManager = itemManager
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItems.count
        }
        return itemManager.doneitem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        if indexPath.section == 0 {
            cell.textLabel?.text = itemManager.toDoItems[indexPath.row].title
        } else {
            cell.textLabel?.text = itemManager.doneitem[indexPath.row].title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int)  -> String? {
        return section == 0 ? "To Do" : "Done"
    }

    
}

